##  关于 
Spring Boot 项目模板。
##  技术栈： 
1. 全局框架：Spring Boot-2.0.0 RELEASE;
2. 持久层框架：MyBatis-3，MyBatis Plus-2.0.8（增强工具）;
4. 页面模板：Thymeleaf-2.1.5.RELEASE ;
5. WEB容器：Jetty ;
6. 接口调试工具：Swagger2: ;
7. 日志门面和实现：logback+slf4j: ;
8. 进程缓存：ehcache: ;
9. JDK ：JDK-1.8;
10. 数据库性能监控：Alibaba Druid

## 启动方式：
在每个要启动的项目中，run/debug SpringBootStarter.main();
